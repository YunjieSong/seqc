
## Load DE results
## load("../data/Results_tophat2.RData")
## load("../data/Results_tophat2_DB_A12345_B12345_2012-09-18.Rdata")
## load("../data/Results_tophat2_DB_A12345_B12345_2012-10-15.Rdata")
load("../data/Results_tophat2_DB_A12345_B12345_2012-11-15.Rdata")

## load cuffdiff
ResCuff <- read.table("../data/gene_expA12345_B12345.txt", stringsAsFactors=FALSE,
                   sep='\t', header=TRUE, row.names=2)

## load count data HTSeq.dat
load("../data/HTSeq.RData")
counts.dat <- matrix(as.numeric(HTSeq.dat), nrow= nrow(HTSeq.dat))
rownames(counts.dat) <- rownames(HTSeq.dat)
colnames(counts.dat) <- colnames(HTSeq.dat)

## remove non-gene entries
## see http://www-huber.embl.de/users/anders/HTSeq/doc/count.html
## for the 'special counters' that are added at the end
counts.dat <- counts.dat[grep('no_feature|ambiguous|too_low_aQual|not_aligned|alignment_not_unique' ,
                              rownames(counts.dat), invert=TRUE),]

kNumOfMethods <- 7
FindZeroCounts <- function(cd, min.cov=0){
  ## Find genes with 0 counts in either
  ## one of the conditions
  ## returns n x 2 boolean matrix
  
  col.index <- grepl("A", colnames(cd))
  zero.a.index <- (rowSums(cd[,col.index]) == 0 & rowSums(cd[,!col.index]) > min.cov) 
  zero.b.index <- (rowSums(cd[,!col.index]) == 0 & rowSums(cd[,col.index])  > min.cov)
  ## zeros <- xor(zero.a.index, zero.b.index)
  return(cbind(zero.a.index, zero.b.index))
}

zeros <- FindZeroCounts(counts.dat, 5)

res.deseq <- results.full[[1]]
res.edger <- results.full[[2]]
res.poseq <- results.full[[3]]
res.limmaQN <- results.full[[4]]
res.limmaVoom <- results.full[[5]]
res.bayseq <- results.full[[6]]

FCMeanVarianceData <- function(norm.cd, res, adj.p.index, ind){
  ## Compute the mean and variance for each dataset
  ## input:
  ## norm.cd - normalized counts by the method
  ##  res- DE results
  ##  adj.p.index - adj p.val column number
  ## ind - boolean index matrix for zero count genes
  ## returns a matrix of mean, var, -log10(pval)
  
  col.index <- grepl("A", colnames(norm.cd))
  ## Means
  sample.a.mean <- rowMeans(norm.cd[names(which(ind[,"zero.a.index"])),!col.index])
  sample.b.mean <- rowMeans(norm.cd[names(which(ind[,"zero.b.index"])),col.index])
  means <- c(sample.a.mean, sample.b.mean)
  ## variance
  sample.a.var <- apply(norm.cd[names(which(ind[,"zero.a.index"])),!col.index], 1, var)
  sample.b.var <- apply(norm.cd[names(which(ind[,"zero.b.index"])),col.index], 1, var)
  variance <- c(sample.a.var, sample.b.var)

  ## -log10(p-val)
  ## for p-val=0  -log10(0) = max(-log10(n_i)) + 1
  sample.a.pval <- -log10(res[names(which(ind[,"zero.a.index"])), adj.p.index])
  sample.a.pval[is.infinite(sample.a.pval)] <-  max(sample.a.pval[is.finite(sample.a.pval)]) +1

  sample.b.pval <- -log10(res[names(which(ind[,"zero.b.index"])), adj.p.index])
  sample.b.pval[is.infinite(sample.b.pval)] <-  max(sample.b.pval[is.finite(sample.b.pval)]) +1
  
  minus.log.pval <- c(sample.a.pval, sample.b.pval)

  dat <- cbind(means, variance,  minus.log.pval)
  rownames(dat) <- names(means)
  colnames(dat) <- c("mean", "variance", "-log10.q-val")

  return(dat)
}

plot.dat <- list()

#################
## DESeq
#################
## plot.dat["DESeq"] <- list(FCMeanVarianceData(res.deseq$counts.scaled, res.deseq$all.res, 3, zeros))
require(DESeq)
res.deseq.norm.counts <- counts(res.deseq$cds) %*% diag(1/sizeFactors(res.deseq$cds))
colnames(res.deseq.norm.counts) <- colnames(counts.dat)
rownames(res.deseq$de) <- res.deseq$de[,1]

plot.dat["DESeq"] <- list(FCMeanVarianceData(res.deseq.norm.counts,
                                             res.deseq$de, 8, zeros))
#################
## edgeR
#################
## res.edger.all <- as.data.frame(res.edger$all.res, stringsAsFactors=FALSE)
## numeric.col <- c("Pval", "FDR","logFC", "Mean_at_cond_condA","Mean_at_cond_condB")
## res.edger.all[,numeric.col] <- apply(res.edger.all[,numeric.col], 2 , function(x) as.numeric(x))
## plot.dat["edgeR"] <- list(FCMeanVarianceData(res.edger$counts.scaled, res.edger.all , 3, zeros))
res.edger.norm.counts <- res.edger$counts$counts %*% diag(1/res.edger$counts$samples$norm.factors)
colnames(res.edger.norm.counts) <- colnames(counts.dat)
plot.dat["edgeR"] <- list(FCMeanVarianceData(res.edger.norm.counts,
                                             res.edger$de$table , 4, zeros))

#################
## limma
#################
plot.dat["limmaQN"] <- list(FCMeanVarianceData(res.limmaQN$counts, res.limmaQN$tab , 2, zeros))
plot.dat["limmaVoom"] <- list(FCMeanVarianceData(res.limmaVoom$counts, res.limmaVoom$tab , 2, zeros))

#################
## PoissonSeq
#################

PoissonNormCounts <- function(cd, lib.factors){
  ## computes PoissonSeq normalized counts
  ## not part of the standard PoissSeq output
  norm.cd <- as.matrix(cd) %*% diag(1/lib.factors)
  rownames(norm.cd) <- rownames(cd)
  colnames(norm.cd) <- colnames(cd)
  
  return(norm.cd)
}

poiss.depth <- PoissonNormCounts(counts.dat, res.poseq$norm.factors)
poiss.matrix <- data.frame(tt=res.poseq$res$tt, pval=res.poseq$res$pval,
                           fdr=res.poseq$res$fdr, logFC=res.poseq$res$log.fc)
rownames(poiss.matrix) <- res.poseq$res$gname

plot.dat["PoissonSeq"] <- list(FCMeanVarianceData(poiss.depth, poiss.matrix, 3, zeros))

#################
## CuffDiff
#################
load("../data/gene_FPKM.Rdata")
fpkm.counts <- res
plot.dat["CuffDiff"] <- list(FCMeanVarianceData(fpkm.counts, ResCuff, 12, zeros))


#################
## baySeq
#################
res.bayseq <- merge(res.bayseq$MA,
                    res.bayseq$de,
                    by.x='row.names', by.y='row.names')
rownames(res.bayseq) <- res.bayseq[,1]
res.bayseq <- res.bayseq[,-1]
plot.dat["baySeq"] <- list(FCMeanVarianceData(res.bayseq[,colnames(counts.dat)],
                                              res.bayseq, 15, zeros))


#################
## Plot away
#################
plot2file=FALSE
if(plot2file){
  pdf(paste("../results/ZeroCountAnalysis_", Sys.Date(), ".pdf", sep=''))
}

## color.function <- colorRampPalette(c("orange", "gray", "red"), space='rgb')
## colr <- color.function(kNumOfMethods)
colr <- c("#A6CEE3", "#1F78B4", "#B2DF8A", "#33A02C", "#FB9A99", "#E31A1C", "#FDBF6F")


PlotFnctI <- function(dat, col, title){
  ## plot mean vs. -log10.q-val
  plot(dat[,"mean"], dat[,"-log10.q-val"], col=col, pch=20,
       main=title,
       xlab="Mean in expressed condition",
       ylab="-log10(Adj. P-value)")
  
}

## sapply(seq(kNumOfMethods), function(i) PlotFnctI(plot.dat[[i]], colr[i], names(plot.dat)[i]))

## 
## lim <- sapply(plot.dat, function(x) max(sqrt(x[is.finite(x[,2]),2])/x[is.finite(x[,1]),1]))
## lim <- max(lim[is.finite(lim)])

PlotFnctCV <- function(dat, col, title){
  ## plot coefficient of variance vs. -log10.q-val
  xx <- sqrt(dat[,"variance"])/dat[,"mean"]
  yy <- dat[,"-log10.q-val"]
  plot(xx, yy,
       col=ifelse(dat[,"-log10.q-val"] > 1.3, col, 'gray'),
       pch=20,
       main=title,
       ## xlim=c(0,2.0),
       xlab=expression(paste("Coefficient of variance (", sigma/mu, ")")),
       ylab="-log10(Adj. P-value)")
  abline(h=-log10(0.05), lwd=2, lty=3)

  sc <- cor(xx,yy, method='kendall')
  mtext(paste("Kendall correlation", format(sc, digits=3), pasete=' '))
  return(sc)
}


PlotFnctSTN <- function(dat, col, title){
  ## plot signal to noise ratios vs. -log10.q-val
  xx <- dat[,"mean"]/sqrt(dat[,"variance"])
  yy <- dat[,"-log10.q-val"]
  plot(xx, yy,
       col=ifelse(dat[,"-log10.q-val"] > 1.3, col, 'gray'),
       pch=20,
       main=title,
       ## xlim=c(0,2.0),
       xlab=expression(paste("Signal-to-noise ratio (", mu/sigma, ")")),
       ylab="-log10(Adj. P-value)")

  abline(h=-log10(0.05), lwd=2, lty=3)
  sc <- cor(xx,yy, method='kendall')
  ## mtext(paste("Kendall correlation", format(sc, digits=3), pasete=' '))
  iso.reg <- isoreg(xx, yy)
  
  lines(iso.reg,
        col= 'gray19',
        pch=10,
        do.points=TRUE,
        lwd=.5, cex=.5
       ## main=title,
       ##xlab=expression(paste("Signal-to-noise ratio (", mu/sigma, ")")),
       ##ylab="-log10(Adj. P-value)"
       )
  return(list(kandell=sc, isotonic=iso.reg))
}

corr.cv <- sapply(seq(kNumOfMethods), function(i) PlotFnctCV(plot.dat[[i]], colr[i], names(plot.dat)[i]))
corr.stn <- sapply(seq(kNumOfMethods), function(i) PlotFnctSTN(plot.dat[[i]], colr[i], names(plot.dat)[i]))

## plot correlation values
## note that values of abs(corr.sv) = corr.stn
## Need only to plot one
colnames(corr.stn) <- names(plot.dat)
barplot(unlist(corr.stn[1,]), col=colr,
        main='Signal-to-Noise vs.-log10(p-val) Kendall-tau correlations',
        ylab="Kendall-tau coefficient",
        ylim=c(0,1),
        cex.names=.75)

## RMSD of Isotonic regression
isotonic.rmsd <- sapply(seq(dim(corr.stn)[2]),
                        function(x) sqrt(sum((corr.stn[2,x][[1]]$yf - corr.stn[2,x][[1]]$y)^2)/table(zeros)["TRUE"]))

## KS test for goodness of fit.
isotonic.ks = sapply(seq(dim(corr.stn)[2]),
  function(x) ks.test(corr.stn[2,x][[1]]$y, corr.stn[2,x][[1]]$yf)$p.value)


names(isotonic.rmsd) <- colnames(corr.stn)
barplot(isotonic.rmsd, col=colr,
        main='RMSD of Isotonic regression',
        ylab='RMSD',
        ylim=c(0,ceiling(max(isotonic.rmsd))),
        cex.names=.75)

##################
## ROC curves
#################
PlotROC <- function(dat, color, method.number, SNCutoff=0){
  require(pROC)
  outcome= rep(1, dim(dat)[1])
  outcome[dat[,"mean"]/sqrt(dat[,"variance"]) <= SNCutoff] =0
  if(method.number==1){
    roc <- plot.roc(outcome, dat[,"-log10.q-val"],col=color,
                    main="ROC of Zero-count genes", ylim=c(0,1.05))
    mtext(paste("Signal-to-Noise cutoff= ", SNCutoff, sep=''), side=3, padj=-1.75, cex=.8)

  }else{
    roc <- lines.roc(outcome, dat[,"-log10.q-val"], add=TRUE, col=color)
  }
  return(roc)
}

#########################
## Test for Monotonicity
## possible use of 'isotone' package
## for general isotone regression
## http://cran.r-project.org/web/packages/isotone/index.html
##
#########################

kSignalToNoise <- 3 ## SN cutoff for ROC
res <- lapply(seq(kNumOfMethods), function(i) PlotROC(plot.dat[[i]], colr[i], i, kSignalToNoise))
names(res) <- names(plot.dat)
legends <- lapply(seq(kNumOfMethods), function(i)
                  paste(names(res)[i], "AUC =", format(res[[i]]$auc, digits=3), sep=' '))

legend("bottomright", legend=legends, col=colr, lwd=2, cex=.75, inset=c(0,0.03))

if(plot2file){
  dev.off()
}


##
## Inspect specific genes
##
means <- sapply(plot.dat, function(x) x[,1])
vars <- sapply(plot.dat, function(x) x[,2])
logpval <- sapply(plot.dat, function(x) x[,3])
stn <- means/sqrt(vars)
cv <- 1/stn
sums <- sapply(plot.dat, rowSums)
maxes <- sapply(plot.dat, rowMax)
temp <- (sums - maxes)/sums

## Select a subset of genes for further inspection
## e.g. all genes with co.variance between 1.8-2.0 and qvale<0.05 from DESeq
xx= c(names(cv[cv[,1] <2.0 & cv[,1]>1.8 & logpval[,1] > 1.3,1]),
  names(means[means[,1] >100 & means[,1] < 800,1] ))

## e.g two genes with similar mean but different q-val in DESeq
##xx= names(means[means[,1] >400 & means[,1] < 500,1] )

## insepct their raw counts, means, vars, cv and q-vals
cat('\n-----------\nRaw counts\n')
print(counts.dat[xx,])
cat('\n-----------\nMeans\n')
print(means[xx,])
cat('\n-----------\nvars counts\n')
print(vars[xx,])
cat('\n-----------\nSignal-to-Noise\n')
print(stn[xx,])
cat('\n-----------\n-log10 pval\n')
print(logpval[xx,])

## 1. find genes with high CV large means rownames(means[cv[,1]>=1.0,])
## 2. inspect means  means[c('UNCX','CALML5','NKX2-6','RNF223'),]
## 3. inspect logpval[c('UNCX','CALML5','NKX2-6','RNF223'),]
## 4. inspect counts.dat 
## 4. Find genes with low CV but similar means means[rownames(means[cv[,1]<.5 & (means[,1]>50 & means[,1]<120 ),]),c(1:3)]
