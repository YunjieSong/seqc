runNOISeq <- function(count.dat, conditions){

    require("NOISeq")
    
    if(! packageDescription("NOISeq")$Version == "1.0.0"){
      stop("Wrong version of DESeq package. This script requires  NOISeq-1.0.0")
    }

    kSamples <- colnames(count.dat)
    targets <- data.frame(samples= kSamples, Factor=conditions)
    
    ## Initialize new NOISeq object
    dd <- readData(count.dat, factors=targets)

    if(length(conditions) == 2){
      inoise <- noiseq(dd, factor="Factor", replicates="no")
    }else{
      inoise <- noiseq(dd,factor="Factor", replicates="technical")
    }

    ## differential expression
    res <- degenes(inoise,q=0.0, M=NULL)

    ## can use 'rpkm' function to obtain normalization counts.
    return(list(dat=inoise, de=res))
}
