#!/usr/bin/perl -w

#tophat2 with bowtie1

$BIN = "/opt/bin/";
$PROJECT=$ARGV[0];
$R1_file=$ARGV[1];
$R2_file=$ARGV[2];


$OUT = "tophat2_bowtie1";
$REF="hg19_ERCC92";
$GTF="hg19_150_ERCC.gtf";
$GTF_index ="hg19_150_ERCC";




open(IN1,"<$R1_file") || die "Can't open $R1_file:$!\n";
open(IN2,"<$R2_file") || die "Can't open $R2_file:$!\n";

@R1_lines=<IN1>;
@R2_lines=<IN2>;
close(IN1);
close(IN2);
$R1=join(",",@R1_lines);
$R1=~ s/\n//g;


$R2=join(",",@R2_lines);
$R2=~ s/\n//g;

#turn on --no-coverage-search search
#system ("qsub -pd alloc 12 -N th-$PROJECT  ~/bin/qCMD /opt/bin/tophat -p 12 --zpacker /opt/pigz-2.1.6/pigz  -r 70 --mate-std-dev 90 --GTF $GTF --transcriptome-index=$GTF_index --no-coverage-search -o /ifs/data/liang/OUT/RNAseq/$PROJECT  $REF $R1 $R2")

system ("qsub -pe alloc 12 -N th2-$PROJECT  ~/bin/qCMD $BIN/tophat -p 12 --zpacker /opt/pigz-2.1.6/pigz  -r 70 --bowtie1 --mate-std-dev 90 --GTF $GTF --transcriptome-index=$GTF_index -o $OUT/$PROJECT  $REF $R1 $R2")

