source("DEanalysisHTseqTopHat2.R")

DEanalysis("A12345_B12345")
DEanalysis("A1234_B1234")
DEanalysis("A123_B123")
DEanalysis("A12_B12")

## Null models
DEanalysis("A12_A345")
DEanalysis("B12_B345")
DEanalysis("A12_A34")
DEanalysis("B12_B34")
DEanalysis("A1_A2")
DEanalysis("A3_A4")
DEanalysis("B1_B2")
DEanalysis("B3_B4")
