Due to size restrictions the data for the downsampling analysis 
is now available here:

    http://qbio.mskcc.org/Public/SocciN/SEQC/encodeDownsample.tar.gz

